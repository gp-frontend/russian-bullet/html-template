"use strict";

module.exports = [
  require("./property_groups/top_properties"),
  require("./property_groups/grid_child"),
  require("./property_groups/box_aligment_child_rules"),
  require("./property_groups/box_model"),
  require("./property_groups/flexbox_rules"),
  require("./property_groups/grid"),
  require("./property_groups/box_aligment_container_rules"),
  require("./property_groups/width"),
  require("./property_groups/height"),
  require("./property_groups/padding"),
  require("./property_groups/margin"),
  require("./property_groups/positioning"),
  require("./property_groups/list_style"),
  require("./property_groups/table_layout"),
  require("./property_groups/border"),
  require("./property_groups/background"),
  require("./property_groups/shadows"),
  require("./property_groups/outline"),
  require("./property_groups/typography"),
  require("./property_groups/transform"),
  require("./property_groups/animation"),
  require("./property_groups/misc")
]
