"use strict";

module.exports = {
  "groupName": "Box alignment container rules",
  "order": "flexible",
  "properties": [
    "align-content",
    "align-items",
    "justify-content",
    "justify-items"
  ]
}
