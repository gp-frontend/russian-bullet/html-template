"use strict";

module.exports = {
  "groupName": "Box model",
  "properties": [
    "display",
    "box-sizing"
  ]
}
