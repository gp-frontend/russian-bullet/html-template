"use strict";

module.exports = {
  "groupName": "Grid",
  "order": "flexible",
  "properties": [
    "grid",
    "grid-auto-rows",
    "grid-auto-columns",
    "grid-auto-flow",
    "grid-gap",
    "grid-row-gap",
    "grid-column-gap",
    "grid-template",
    "grid-template-areas",
    "grid-template-rows",
    "grid-template-columns",
    "gap"
  ]
}
