"use strict";

module.exports = {
  "groupName": "List style",
  "properties": [
    "list-style",
    "list-style-position",
    "list-style-type",
    "list-style-image"
  ]
}
