"use strict";

global.$ = {
  gulp: require('gulp'),
  browserSync: require('browser-sync').create(),
  plugins: require('gulp-load-plugins')(), // allow used plugins without vars,
  path: {
    tasks: require('./config/gulp/config_tasks.js')
  },
  del: require('del')
};

// Позволяет разбить gulp tasks на отдельные файлы
$.path.tasks.forEach(function (taskPath) {
  require(taskPath)();
});

$.gulp.task('scripts:build', $.gulp.series(

  $.gulp.parallel(
    'scripts:dev',
    'libsJs:dev',
  )
));

$.gulp.task('styles:generate', $.gulp.series(

  $.gulp.parallel(
    'styles:normalize',
    'styles:bootstrap',
  )
));

$.gulp.task('dev', $.gulp.series(
  'clean',
  $.gulp.parallel(
    'pug',
    'styles:generate',
    'styles:dev',
    'scripts:build',
    'images:copy',
    'svg:sprite',
    'svg:base',
    'json:svg',
    'json:merge',
    'copy',
  )
));

$.gulp.task('prod', $.gulp.series(
  'clean',
  $.gulp.parallel(
    'pug',
    'styles:generate',
    'styles:prod',
    'scripts:prod',
    'libsJs:prod',
    'images:compress',
    'images:svg',
    'images:gif',
    'svg:sprite',
    'svg:base',
    'json:svg',
    'json:merge',
    'copy',
  )
));

$.gulp.task('default', $.gulp.series(
  'dev',
  $.gulp.parallel('watch', 'serve')
));
